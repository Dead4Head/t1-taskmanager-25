package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    void login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout();

    boolean isAuth();

    @Nullable
    String getUserId() throws AbstractException;

    @NotNull
    User getUser() throws AbstractException;

    void checkRoles(Role[] roles) throws AbstractException;

}
