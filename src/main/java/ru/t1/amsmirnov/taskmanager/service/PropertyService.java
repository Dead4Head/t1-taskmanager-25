package ru.t1.amsmirnov.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "author.name";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "3228";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "SeCrEt";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    public @NotNull String getApplicationVersion() {
        return getStringValue(APPLICATION_VERSION_KEY);
    }

    @Override
    public @NotNull String getAuthorName() {
        return getStringValue(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getAuthorEmail() {
        return getStringValue(AUTHOR_EMAIL_KEY);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}
