package ru.t1.amsmirnov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @Nullable
    M add(@NotNull M model);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull Collection<M> collection);

    @Nullable
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull Integer index);

    int getSize();

    boolean existById(@NotNull String id);

}
